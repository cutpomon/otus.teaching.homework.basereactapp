import { Component } from "react";
import "../Styles/Authorization.css";
import FormItem from "./FormItem";

class Authorization extends Component {
	constructor(props) {
		super(props);
		this.state = { persons: [] };
	}

	handleOnAuthorization = async () => {
		await fetch("/users")
			.then((res) => res.json())
			.then((json) => {
				console.log(json);
				this.setState({ persons: json });
			})
			.catch((error) => {
				console.log(error);
			});
	};

	render() {
		return (
			<div className="Authorization">
				<h1> Вход в систему </h1>
				<FormItem label="Логин"></FormItem>
				<FormItem label="Пароль" type="password"></FormItem>
				<button onClick={this.handleOnAuthorization}> Войти</button>
				<div>
					Данные, полученные по API:
					{this.state.persons.map((person) => (
						<li key={person.id}> {person.name} </li>
					))}
				</div>
			</div>
		);
	}
}

export default Authorization;
