import "../Styles/FormItem.css";

function FormItem(props) {
	return (
		<div className="formItem">
			<label className="formItem__label"> {props.label} </label>
			<input type={props.type ?? "text"} onChange={props.handleChange} />
		</div>
	);
}

export default FormItem;
