import { Link } from "react-router-dom";
import "../Styles/NavBar.css";

function NavBar() {
	return (
		<div className="NavBar">
			<div className="navbar-container">
				<nav>
					<ul>
						<li>
							<Link to="/" className="navbar-link">
								Главная
							</Link>
						</li>
						<li>
							<Link to="/login" className="navbar-link">
								Войти
							</Link>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	);
}

export default NavBar;
