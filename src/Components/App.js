import "../Styles/App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Authorization from "./Authorization";
import Home from "./Home";
import NavBar from "./NavBar";

function App() {
	return (
		<div className="App">
			<Router>
				<NavBar />
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/login" element={<Authorization />} />
				</Routes>
			</Router>
		</div>
	);
}

export default App;
